package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public static final int COMPRESSOR_PORT = 0;

  public final class DrivetrainConstants {
    public static final int LEFT_MOTOR_PORT = 0;
    public static final int RIGHT_MOTOR_PORT = 1;
  }

  public final class IndexerConstants {
    public static final int INDEXER_MOTOR_PORT = 2;
  }

  public final class ArmConstants {
    public static final int DOUBLE_SOLENOID_0 = 0;
    public static final int DOUBLE_SOLENOID_1 = 1;
  }
}
