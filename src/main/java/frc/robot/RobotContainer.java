package frc.robot;

import com.team6479.lib.commands.TeleopTankDrive;
import com.team6479.lib.controllers.CBXboxController;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.ToggleIndexer;
import frc.robot.subsystems.Arm;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Indexer;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {

  public final Drivetrain drivetrain = new Drivetrain();
  public final Indexer indexer = new Indexer();
  public final Arm arm = new Arm();

  public static Compressor compressor = new Compressor(Constants.COMPRESSOR_PORT);

  public final CBXboxController xbox = new CBXboxController(0);

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    drivetrain.setDefaultCommand(new TeleopTankDrive(drivetrain, () -> xbox.getY(Hand.kLeft), () -> xbox.getX(Hand.kRight)));

    xbox.getButton(Button.kA)
        .whenPressed(new InstantCommand(indexer::run, indexer))
        .whenReleased(new InstantCommand(indexer::stop, indexer));

    xbox.getButton(Button.kB)
        .whenPressed(new ToggleIndexer(indexer));

    xbox.getButton(Button.kX)
        .whenPressed(new SequentialCommandGroup(
            new InstantCommand(indexer::run, indexer),
            new WaitCommand(5),
            new InstantCommand(indexer::stop, indexer)
        ));
    xbox.getButton(Button.kY)
        .whenPressed(new InstantCommand(arm::toggle, arm));
  }
}
