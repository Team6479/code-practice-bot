package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IndexerConstants;

public class Indexer extends SubsystemBase {
  private final TalonSRX indexerMotor;

  private boolean isOn;

  public Indexer() {
    indexerMotor = new TalonSRX(IndexerConstants.INDEXER_MOTOR_PORT);

    indexerMotor.configFactoryDefault();

    indexerMotor.setNeutralMode(NeutralMode.Brake);

    indexerMotor.setInverted(false);

    isOn = false;
  }

  public void stop() {
    indexerMotor.set(ControlMode.PercentOutput, 0);
    isOn = false;
  }

  public void run() {
    indexerMotor.set(ControlMode.PercentOutput, 0.5);
    isOn = true;
  }

  public void reverse() {
    indexerMotor.set(ControlMode.PercentOutput, -0.5);
    isOn = true;
  }

  public boolean getIsOn() {
    return isOn;
  }
}
