package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Arm extends SubsystemBase {
  private final DoubleSolenoid armSol = new DoubleSolenoid(Constants.ArmConstants.DOUBLE_SOLENOID_0, Constants.ArmConstants.DOUBLE_SOLENOID_1);

  public Arm() {
    retract();
  }

  public void extend() {
    armSol.set(DoubleSolenoid.Value.kForward);
  }

  public void retract() {
    armSol.set(DoubleSolenoid.Value.kReverse);
  }

  public void toggle() {
    armSol.set((armSol.get() == DoubleSolenoid.Value.kForward) ? DoubleSolenoid.Value.kReverse : DoubleSolenoid.Value.kForward);
  }
}
