package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.team6479.lib.subsystems.TankDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DrivetrainConstants;

public class Drivetrain extends SubsystemBase implements TankDrive {
  private final TalonSRX leftMotor = new TalonSRX(DrivetrainConstants.LEFT_MOTOR_PORT);
  private final TalonSRX rightMotor = new TalonSRX(DrivetrainConstants.RIGHT_MOTOR_PORT);

  public Drivetrain() {
    leftMotor.configFactoryDefault();
    rightMotor.configFactoryDefault();

    leftMotor.setNeutralMode(NeutralMode.Brake);
    rightMotor.setNeutralMode(NeutralMode.Brake);

    leftMotor.setInverted(false);
    rightMotor.setInverted(true);
  }

  public void stop() {
    leftMotor.set(ControlMode.PercentOutput, 0);
    rightMotor.set(ControlMode.PercentOutput, 0);
  }

  public void tankDrive(double leftSpeed, double rightSpeed) {
    leftMotor.set(ControlMode.PercentOutput, leftSpeed);
    rightMotor.set(ControlMode.PercentOutput, rightSpeed);
  }

  public void arcadeDrive(double forward, double rotation) {
    leftMotor.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +rotation);
    rightMotor.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -rotation);
  }
}
