package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.Indexer;

public class ToggleIndexer extends InstantCommand {
  private final Indexer indexer;

  public ToggleIndexer(Indexer indexer) {
    this.indexer = indexer;
    addRequirements(this.indexer);
  }

  @Override
  public void initialize() {
    if (indexer.getIsOn()) {
      indexer.stop();
    } else {
      indexer.run();
    }
  }
}
